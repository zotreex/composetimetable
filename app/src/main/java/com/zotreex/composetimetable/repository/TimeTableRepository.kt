package com.zotreex.composetimetable.repository

import com.zotreex.composetimetable.data.database.TimeTableDatabaseDao
import com.zotreex.composetimetable.data.remote.TableApi
import com.zotreex.composetimetable.data.remote.TableResponse
import com.zotreex.composetimetable.util.Resource
import dagger.hilt.android.scopes.ActivityScoped
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@ActivityScoped
class TimeTableRepository @Inject constructor(
    private val api: TableApi,
    private val db: TimeTableDatabaseDao
) {
    suspend fun getTableList(code: Int): Resource<String> {
        return try {
            val response = api.getTableList(code)
            db.insert(response)
            Resource.Success("OK")
        } catch (e: Exception) {
            Timber.e(e.toString())
            Resource.Error(e.message ?: "Connection error")
        }
    }

    fun classesFlow() : Flow<List<TableResponse>> {
        return db.getAll()
    }

    suspend fun clear() {
        db.clear()
    }

    fun hasClasses() : Boolean = db.countClasses() > 0

    suspend fun requestNewClasses(code: Int): Resource<String>{
        return try {
            val response = api.getTableList(code)
            db.clear()
            db.insert(response)
            Resource.Success("OK")
        } catch (e: Exception) {
            Timber.e(e.toString())
            Resource.Error("Connection error")
        }
    }
}
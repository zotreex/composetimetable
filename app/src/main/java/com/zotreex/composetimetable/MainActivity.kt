package com.zotreex.composetimetable

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.CalendarToday
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.*
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.zotreex.composetimetable.navigation.Screen
import com.zotreex.composetimetable.screens.classesDetail.ClassesDetailScreen
import com.zotreex.composetimetable.screens.classesList.ClassesListScreen
import com.zotreex.composetimetable.screens.import.ImportScreen
import com.zotreex.composetimetable.screens.settings.SettingsScreen
import com.zotreex.composetimetable.ui.theme.ComposeTimeTableTheme
import com.zotreex.composetimetable.ui.theme.colorSelected
import com.zotreex.composetimetable.ui.theme.colorUnselected
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    @ExperimentalAnimationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val systemUiController = rememberSystemUiController()
            if(isSystemInDarkTheme()) {
                systemUiController.setStatusBarColor(Color(0xFF1D1D1D), darkIcons = false)
                systemUiController.setNavigationBarColor(Color(0xFF121212), darkIcons = false)
            }
            else
                systemUiController.setSystemBarsColor(Color(0xFFFFFFFF), darkIcons = true)
            ComposeTimeTableTheme {
                BuildApp()
            }
        }
    }
}

@ExperimentalAnimationApi
@Composable
fun BuildApp() {
    val navController = rememberNavController()
    val navBackStackEntry by navController
        .currentBackStackEntryAsState()
    val currentRoute = navBackStackEntry
        ?.destination?.route

    Scaffold(bottomBar =
    {
        if(currentRoute == Screen.ClassesList.route)
        BottomAppBar(cutoutShape = CircleShape, backgroundColor = MaterialTheme.colors.surface,  contentPadding = PaddingValues(0.dp, 0.dp)) {
            BottomNavigation() {

                BottomNavigationItem(
                    modifier = Modifier.background(MaterialTheme.colors.background),
                    unselectedContentColor = colorUnselected(isSystemInDarkTheme()),
                    selectedContentColor = colorSelected(isSystemInDarkTheme()),
                    icon = {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = ""
                        )
                    },
                    selected = true,
                    onClick = {

                    }
                )
                BottomNavigationItem(
                    modifier = Modifier.background(MaterialTheme.colors.background),
                    unselectedContentColor = colorUnselected(isSystemInDarkTheme()),
                    selectedContentColor = colorSelected(isSystemInDarkTheme()),
                    icon = {
                        Icon(
                            imageVector = Icons.Default.CalendarToday,
                            contentDescription = ""
                        )
                    },
                    selected = true,
                    onClick = {

                    }
                )
            }
        }

    },
        floatingActionButtonPosition = FabPosition.Center,
        isFloatingActionButtonDocked = true,
        floatingActionButton = {
            if(currentRoute == Screen.ClassesList.route)
            ExtendedFloatingActionButton(
                shape = CircleShape,
                text = { Text("Сегодня", modifier = Modifier.padding(8.dp),) },
                onClick = { /*do something*/ },
                backgroundColor = MaterialTheme.colors.primary
            )
        }

    ) {
        navigationInit(navController)
    }
}

@ExperimentalAnimationApi
@Composable
fun navigationInit(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.ClassesList.route
    ) {
        composable(Screen.ClassesList.route) {
            EnterAnimation {ClassesListScreen(navController = navController)}
        }
        composable(Screen.Settings.route) {
            EnterAnimation{SettingsScreen(navController = navController)}
        }
        composable(Screen.Import.route) {
            EnterAnimation { ImportScreen(navController = navController) }
        }
        composable(
            "class_detail_screen/{classId}",
            arguments = listOf(
                navArgument("classId") {
                    type = NavType.IntType
                }
            )
        ) {
            val classId = remember {
                it.arguments?.getInt("classId") ?: 0
            }
            EnterAnimation { ClassesDetailScreen(classId = classId, navController = navController) }

        }
    }
}

@ExperimentalAnimationApi
@Composable
fun EnterAnimation(content: @Composable () -> Unit) {
    AnimatedVisibility(
        visible = true,
        enter = fadeIn(initialAlpha = 0.3f),
        exit = fadeOut(),
        initiallyVisible = false
    ) {
        content()
    }
}

package com.zotreex.composetimetable.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.zotreex.composetimetable.data.remote.TableResponse
import kotlinx.coroutines.flow.Flow

@Dao
interface TimeTableDatabaseDao {
    @Insert
    suspend fun insert(item: List<TableResponse>)

    @Query("SELECT * FROM Classes LIMIT 4")
    fun getAll() : Flow<List<TableResponse>>

    @Query("SELECT * FROM Classes where id = :id")
    fun getById(id : Int) : TableResponse

    @Query("SELECT COUNT(*) FROM Classes")
    fun countClasses() : Int

    @Query("DELETE FROM Classes")
    suspend fun clear()
}
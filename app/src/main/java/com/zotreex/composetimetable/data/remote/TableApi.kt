package com.zotreex.composetimetable.data.remote

import retrofit2.http.GET
import retrofit2.http.Path

interface TableApi {
    @GET("timetable/{id}")
    suspend fun getTableList(
        @Path("id") code : Int
    ) : List<TableResponse>
}
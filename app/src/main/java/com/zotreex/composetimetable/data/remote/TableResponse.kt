package com.zotreex.composetimetable.data.remote

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Classes")
data class TableResponse(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val group_id: String,
    val day: String,
)

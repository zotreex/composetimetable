package com.zotreex.composetimetable.data.remote.models

import androidx.compose.ui.graphics.Color

data class ClassesListEntry(
    val id: Int,
    val color: Color,
    var current: Boolean = false,
    var filling: Float = 0f
)

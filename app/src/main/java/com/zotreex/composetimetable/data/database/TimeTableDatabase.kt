package com.zotreex.composetimetable.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.zotreex.composetimetable.data.remote.TableResponse

@Database(
    entities = [
        TableResponse::class
    ],
    version = 1
)
abstract class TimeTableDatabase : RoomDatabase() {
    abstract fun timeTableDatabaseDao(): TimeTableDatabaseDao

    companion object {
        private var INSTANCE: TimeTableDatabase? = null

        fun getInstance(context: Context): TimeTableDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        TimeTableDatabase::class.java,
                        "TimeTableDatabase"
                    ).fallbackToDestructiveMigration()
                        .build()

                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
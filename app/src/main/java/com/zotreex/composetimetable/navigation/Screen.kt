package com.zotreex.composetimetable.navigation

import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.ui.graphics.vector.ImageVector
import com.zotreex.composetimetable.R

sealed class Screen(
    val route: String,
    @StringRes val resourceId: Int,
    val icon: ImageVector = Icons.Default.Home
    ) {
    object ClassesList: Screen("classes_list", R.string.classes_list_screen, Icons.Default.List)
    object ClassesDetail: Screen("classes_detail", R.string.classes_detail_edit_screen)
    object Import: Screen("import", R.string.import_screen, Icons.Default.ImportExport)
    object Settings: Screen("setting", R.string.setting_screen, Icons.Default.Settings)
}
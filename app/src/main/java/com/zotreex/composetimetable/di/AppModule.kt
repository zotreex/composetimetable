package com.zotreex.composetimetable.di

import android.content.Context
import com.zotreex.composetimetable.data.database.TimeTableDatabase
import com.zotreex.composetimetable.data.database.TimeTableDatabaseDao
import com.zotreex.composetimetable.data.remote.TableApi
import com.zotreex.composetimetable.repository.TimeTableRepository
import com.zotreex.composetimetable.util.Constants.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideTimeTableRepository(
        api: TableApi,
        db : TimeTableDatabaseDao
    ) = TimeTableRepository(api, db)

    @Singleton
    @Provides
    fun provideTableApi() : TableApi {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(TableApi::class.java)
    }

    @Singleton
    @Provides
    fun provideDB(@ApplicationContext context: Context) : TimeTableDatabase = TimeTableDatabase.getInstance(context)

    @Provides
    @Singleton
    fun provideTimeTableDao(db: TimeTableDatabase) : TimeTableDatabaseDao = db.timeTableDatabaseDao()

    @ApplicationScope
    @Provides
    @Singleton
    fun providesApplicationScope() = CoroutineScope(SupervisorJob())
}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope
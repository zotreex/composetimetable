package com.zotreex.composetimetable.screens.classesList

import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zotreex.composetimetable.data.remote.TableResponse
import com.zotreex.composetimetable.data.remote.models.ClassesListEntry
import com.zotreex.composetimetable.repository.TimeTableRepository
import com.zotreex.composetimetable.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@HiltViewModel
class ClassesListViewModel @Inject constructor(
    private val repository : TimeTableRepository
): ViewModel() {

    var classesList = mutableStateOf<List<ClassesListEntry>>(listOf())
    var loadError = mutableStateOf("")
    var isLoading = mutableStateOf(false)

    init {
        loadClasses()
    }

    private fun loadClasses() {
        viewModelScope.launch {
            repository.classesFlow().collect{ list->
                classesList.value = findLive(list)
            }
        }
    }

    fun findLive(list : List<TableResponse>) : List<ClassesListEntry>{
        var tmp = mutableListOf<ClassesListEntry>()
        list.forEachIndexed { index, tableResponse ->
            if(index == 1) {
                tmp.add(ClassesListEntry(id = tableResponse.id,current = true,color = randColor()))
            } else
            tmp.add(ClassesListEntry(id = tableResponse.id, randColor()))

        }
        return tmp
    }

    private fun randColor(): Color {
        return Color((30..200).random(),(30..200).random(),(30..200).random())
    }

    var fastBack = mutableStateOf(false)

    fun getClasses(code: Int){
        viewModelScope.launch {
            isLoading.value = true
            when(val request = repository.requestNewClasses(code)){
                is Resource.Success -> {
                    isLoading.value = false
                    fastBack.value = true
                }
                is Resource.Error -> {
                    isLoading.value = false
                    loadError.value = request.message ?: ""
                }
                is Resource.Loading -> {isLoading.value = true}
            }
        }
    }
}
package com.zotreex.composetimetable.screens.classesList

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Circle
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.zotreex.composetimetable.data.remote.models.ClassesListEntry
import com.zotreex.composetimetable.navigation.Screen
import com.zotreex.composetimetable.ui.theme.colorSeparator

@Composable
fun ClassesListScreen(
    navController: NavController
){
    Surface(
        modifier = Modifier.fillMaxSize()
    ) {
        Column() {
            DateTitle(navController)
            Spacer(modifier = Modifier.height(32.dp))
            Card(shape = RoundedCornerShape(16.dp)) {
                ClassesList(navController)
            }

        }
    }
}

@Composable
fun DateTitle(navController: NavController) {
    Card(
        modifier = Modifier
            .fillMaxWidth(),
        shape = RoundedCornerShape(bottomEnd = 16.dp, bottomStart = 16.dp),
        backgroundColor = MaterialTheme.colors.background
    )
    {
        Row(
            modifier = Modifier.padding(32.dp, 32.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {

            Row() {
                Text(text = "1 Июня, ", style = MaterialTheme.typography.h1)
                Text(text = "Пн", style = MaterialTheme.typography.h1)
            }

            Column(modifier = Modifier.fillMaxWidth()) {
                IconButton(onClick = {
                    navController.navigate(Screen.Settings.route){
                        launchSingleTop = true
                        restoreState = true
                    }
                }, modifier = Modifier.align(Alignment.End)) {
                    Icon(Icons.Filled.Settings, "Settings")
                }
            }
        }
    }
}

@Composable
fun ClassesEntry(
    entry: ClassesListEntry,
    navController: NavController,
    modifier: Modifier = Modifier,
    viewModel: ClassesListViewModel = hiltViewModel()
){
    val color = if(entry.current) entry.color.copy(alpha = 0.24f) else MaterialTheme.colors.background
    Box(modifier = modifier
        .background(color)
        .clickable {
            navController.navigate(
                "class_detail_screen/${entry.id}"
            )
        }
    ) {
        Column(modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp)
        ) {
            Row(modifier = Modifier
                .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ){
                Box()
                {
                    Icon(
                        Icons.Filled.Circle,
                        "",
                        modifier = Modifier.size(12.dp),
                        tint = entry.color
                    )

                }
                Spacer(Modifier.width(4.dp))
                Text(
                    fontWeight = FontWeight.Bold,
                    style = MaterialTheme.typography.body1,
                    text = entry.id.toString() + "Название предмета"
                )
            }
            Spacer(Modifier.height(4.dp))
            Text(
                text = "A-17"
            )
            Spacer(Modifier.height(4.dp))
            Text(
                text = "4:00 - 4:30"
            )
        }
    }
}

@Composable
fun ClassesList(
    navController: NavController,
    viewModel: ClassesListViewModel = hiltViewModel()
){
    val classesList by remember { viewModel.classesList }
    val loadError by remember { viewModel.loadError }
    val isLoading by remember { viewModel.isLoading }
    
    Text(text = loadError)
    Loading(show = isLoading)
    LazyColumn() {
        items(classesList.size){
            ClassesRow(entry = classesList[it], navController = navController)
            if(it != classesList.size-1)Divider(color = colorSeparator(isSystemInDarkTheme()), thickness = 1.dp)
        }
    }
}

@Composable
fun Loading(show: Boolean){
    if(show)
        Row(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
            Arrangement.Center
        ){
            CircularProgressIndicator(
                color = MaterialTheme.colors.primary
            )
        }
}

@Composable
fun ClassesRow(
    entry : ClassesListEntry,
    navController: NavController
){
    Column() {
        Row{
            ClassesEntry(entry = entry, navController = navController)
        }
    }
}
package com.zotreex.composetimetable.screens.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.zotreex.composetimetable.navigation.Screen
import com.zotreex.composetimetable.screens.classesList.ClassesListViewModel
import com.zotreex.composetimetable.screens.classesList.Loading
import com.zotreex.composetimetable.util.ToolbarWidget

@Composable
fun SettingsScreen(
    navController: NavController,
    viewModel: SettingsViewModel = hiltViewModel()
){
    Surface(modifier = Modifier.fillMaxSize()){
        Column() {
            val textState = remember { mutableStateOf(TextFieldValue()) }
            val loadError by remember { viewModel.loadError }
            val isLoading by remember { viewModel.isLoading }
            val fastBack by remember { viewModel.fastBack }
            val code by remember { viewModel.code }

            FastBack(
                fastBack = fastBack,
                navController = navController
            )

            ToolbarWidget(
                title = stringResource(Screen.Settings.resourceId),
                navController = navController
            )
            Button(
                onClick = {
                    viewModel.clearCache()
                }, modifier = Modifier
                    .fillMaxWidth()
                    .padding(16.dp)
            ) {
                Text(text = "Очистить")
            }

            TextFieldDemo(textState)
            SaveButton(textState)
            Text(text=code)
            Text(text=loadError)
            Loading(show = isLoading)
        }
    }

}

@Composable
fun FastBack(fastBack: Boolean, navController: NavController){
    if(fastBack) navController.popBackStack()
}

@Composable
fun SaveButton(textState: MutableState<TextFieldValue>, viewModel: SettingsViewModel = hiltViewModel()) {
    Button(onClick = {
        textState.value.text.let {
            viewModel.getClasses(it.toInt())
        }
    }, modifier = Modifier
        .fillMaxWidth()
        .padding(16.dp)) {
        Text(text = "Получить")
    }
}

@Composable
fun TextFieldDemo(textState: MutableState<TextFieldValue>) {
    OutlinedTextField(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        label = {Text("Код расписания")},
        value = textState.value,
        onValueChange = { textState.value = it }
    )
}
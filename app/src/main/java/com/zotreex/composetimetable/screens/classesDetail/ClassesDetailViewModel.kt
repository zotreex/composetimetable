package com.zotreex.composetimetable.screens.classesDetail

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import com.zotreex.composetimetable.data.remote.models.ClassesListEntry
import com.zotreex.composetimetable.repository.TimeTableRepository
import com.zotreex.composetimetable.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import androidx.compose.ui.graphics.Color

@HiltViewModel
class ClassesDetailViewModel @Inject constructor(
    private val repository: TimeTableRepository
) : ViewModel() {

    var classesInfo = mutableStateOf<List<ClassesListEntry>>(listOf())
    suspend fun getClass(id: Int) : Resource<ClassesListEntry> {
        return Resource.Success<ClassesListEntry>(ClassesListEntry(id, Color(0xFF6573c3)))
    }
}
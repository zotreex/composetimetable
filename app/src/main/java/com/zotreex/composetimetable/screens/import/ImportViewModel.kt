package com.zotreex.composetimetable.screens.import

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zotreex.composetimetable.repository.TimeTableRepository
import com.zotreex.composetimetable.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImportViewModel @Inject  constructor(
    private val repository: TimeTableRepository
): ViewModel() {
    var isLoading = mutableStateOf(false)
    var loadError = mutableStateOf("")
    var fastBack = mutableStateOf(false)

    fun getClasses(code: Int){
        viewModelScope.launch {
            isLoading.value = true
            when(val request = repository.requestNewClasses(code)){
                is Resource.Success -> {
                    isLoading.value = false
                    fastBack.value = true
                }
                is Resource.Error -> {
                    isLoading.value = false
                    loadError.value = request.message ?: ""
                }
                is Resource.Loading -> {isLoading.value = true}
            }
        }
    }
}
package com.zotreex.composetimetable.screens.classesDetail

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.zotreex.composetimetable.data.remote.models.ClassesListEntry
import com.zotreex.composetimetable.util.Resource
import com.zotreex.composetimetable.util.ToolbarWidget

@Composable
fun ClassesDetailScreen(
    classId: Int,
    navController: NavController,
    viewModel: ClassesDetailViewModel = hiltViewModel()
){
    val classesInfo by remember { viewModel.classesInfo }

    Column() {
        ToolbarWidget("Редактирование", navController)
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            Text(text = classId.toString())
        }
    }

}
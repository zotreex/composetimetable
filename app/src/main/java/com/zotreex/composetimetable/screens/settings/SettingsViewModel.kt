package com.zotreex.composetimetable.screens.settings

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zotreex.composetimetable.repository.TimeTableRepository
import com.zotreex.composetimetable.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val repository: TimeTableRepository
) : ViewModel(){
    var isLoading = mutableStateOf(false)
    var loadError = mutableStateOf("")
    var fastBack = mutableStateOf(false)
    var code = mutableStateOf("")

    init {
        askCode()
    }

    fun getClasses(code: Int){
        viewModelScope.launch {
            //dataStoreManager.setThemeMode(code)
            isLoading.value = true
            when(val request = repository.requestNewClasses(code)){
                is Resource.Success -> {
                    isLoading.value = false
                    fastBack.value = true
                }
                is Resource.Error -> {
                    isLoading.value = false
                    loadError.value = request.message ?: ""
                }
                is Resource.Loading -> {isLoading.value = true}
            }
        }
    }

    private fun askCode() = viewModelScope.launch {
//        dataStoreManager.themeMode().collect {
//            code.value = it.toString()
//        }
    }

    fun clearCache() {
        viewModelScope.launch{ repository.clear() }
    }
}
package com.zotreex.composetimetable.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

private val DarkColorPalette = darkColors(
    primary = DarkPrimary,
    secondary = DeepOrange,
    onPrimary = DarkSurface,
    onSecondary = White,
    primaryVariant = DarkPrimaryVariant,
    error = Red,
    surface = DarkSurface,
    onSurface = White,
    background = DarkSurface,
    onBackground = White,
)

private val LightColorPalette = lightColors(
    primary = Blue,
    secondary = DeepOrange,
    onPrimary = White,
    onSecondary = White,
    primaryVariant = BlueLight,
    error = Red,
    surface = LightSurface,
    onSurface = Black,
    background = LightBackground,
    onBackground = Black
)

@Composable
fun ComposeTimeTableTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable() () -> Unit
) {
    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
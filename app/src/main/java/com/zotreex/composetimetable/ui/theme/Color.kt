package com.zotreex.composetimetable.ui.theme

import androidx.compose.ui.graphics.Color

val White = Color(0xFFFFFFFF)
val BlueDark = Color(0xFF2c387e)
val DarkSurface = Color(0xFF121212)
val Red = Color(0xff9b0000)
val DarkPrimary = Color(0xFFf4f6f7)
val DarkPrimaryVariant = Color(0xFF9299a1)
val DarkSeparator = Color(0XFF1c1c1c)
val LightSeparator = Color(0xFFF1F1F1)

val LightSurface = Color(0xFFF5F5F5)
val LightBackground = Color(0xFFFFFFFF)
val Black = Color(0xFF000000)

fun colorSeparator(dark : Boolean) = if(dark) DarkSeparator else LightSeparator
fun colorSelected(dark: Boolean) = if (dark) DarkPrimary else Blue
fun colorUnselected(dark: Boolean) = if (dark) DarkPrimaryVariant else BlueLight

val Blue = Color(0xFF3f51b5)
val BlueLight = Color(0xFF6573c3)
val DeepOrangeDark = Color(0xFFb22a00)
val DeepOrange = Color(0xFFff3d00)
val DeepOrangeLight = Color(0xFFff6333)

